﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace DailyTheater
{
    static class Program
    {
        /// <summary>
        /// 해당 애플리케이션의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Add exception handler
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += ApplicationThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException; ;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void ApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowExceptionMessage(e.Exception);
        }

        private static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowExceptionMessage(e.ExceptionObject as Exception);
        }

        private static void ShowExceptionMessage(Exception e)
        {
            if (e.GetType() == typeof(NotImplementedException))
                MessageBox.Show("미구현 항목입니다.");
            else
                MessageBox.Show(e.Message);
        }
    }
}
