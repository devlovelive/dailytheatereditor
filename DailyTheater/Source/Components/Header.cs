﻿using DailyTheater.Helpers;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class Header
    {
        public Header()
        {
            // Initialize
            image = new PictureBox()
            {
                Size = new Size(782, 535),
                Location = new Point(0, 25),
                Image = Properties.Resources.background,
                SizeMode = PictureBoxSizeMode.StretchImage,
            };
            title = new Label()
            {
                Name = "title",
                Font = Globals.titleFont,
                ForeColor = Color.FromArgb(0x4a, 0x4a, 0x4a),
                BackColor = Color.FromArgb(0xfc, 0xfc, 0xfe),
                Location = new Point(0, 75),
                Size = new Size(784, 20),
                TabIndex = 3,
                TextAlign = ContentAlignment.MiddleCenter,
                BorderStyle = BorderStyle.None,
                FlatStyle = FlatStyle.Flat,
            };

            // Register events
            title.Paint += (s, e) =>
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            };
        }

        public void Expand(int ds)
        {
            Renderer.Expand(image, ds, ds);
            Renderer.Expand(title, ds, ds);
        }

        public void Shrink(int ds)
        {
            Renderer.Shrink(image, ds, ds);
            Renderer.Shrink(title, ds, ds);
        }

        public PictureBox Image { get { return image; } }

        public Label Title { get { return title; } }

        private PictureBox image;

        private readonly Label title;
    };
}
