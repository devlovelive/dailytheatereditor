﻿using DailyTheater.Helpers;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class Timeline
    {
        public Timeline(Size Size)
        {
            // Initialize
            dialogs = new FlowLayoutPanel()
            {
                Location = new Point(40, 120),
                Size = new Size(710, 330),
                BackColor = Color.FromArgb(0xfc, 0xfc, 0xfe),
            };
            dialogs.HorizontalScroll.Maximum = 0;
            dialogs.VerticalScroll.Visible = false;
            dialogs.AutoScroll = true;

            var dialogInput = new DialogInput(this, Size.Width, Size.Height);

            // Register events
            dialogs.ControlAdded += TimelineControlAdded;

            // Add controls
            dialogs.Controls.Add(dialogInput.pictureBox);

            dialogInput.characterButton.BringToFront();
            dialogInput.text.Current.BringToFront();
        }

        public void Expand(int ds)
        {
            Renderer.ForEach(dialogs, ds, ds, Renderer.Expand);
        }

        public void Shrink(int ds)
        {
            Renderer.ForEach(dialogs, ds, ds, Renderer.Shrink);
            dialogs.HorizontalScroll.Maximum = 0;
            dialogs.AutoScroll = false;
            dialogs.VerticalScroll.Visible = false;
            dialogs.AutoScroll = true;
        }

        private void TimelineControlAdded(object sender, ControlEventArgs e)
        {
            dialogs.ScrollControlIntoView(dialogs.Controls[dialogs.Controls.Count - 1]);
        }

        public FlowLayoutPanel Dialogs { get { return dialogs; } }

        public string LastSpeeker
        {
            get
            {
                if (dialogs.Controls.Count < 2)
                    return null;
                return DialogInput.Get<CharacterButton>(dialogs.Controls[dialogs.Controls.Count - 2], "dialog").ComboBoxName;
            }
        }

        private FlowLayoutPanel dialogs;
    };

}
