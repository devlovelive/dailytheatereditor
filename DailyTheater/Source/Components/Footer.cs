﻿using DailyTheater.Helpers;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class Footer
    {
        public Footer()
        {
            image = new PictureBox()
            {
                Size = new Size(782, 535),
                Location = new Point(0, 25),
                Image = Properties.Resources.background,
                SizeMode = PictureBoxSizeMode.StretchImage,
            };
            save_export_button = new Button()
            {
                Size = new Size(250, 35),
                Location = new Point(266, 516),
            };
        }

        public void Expand(int ds)
        {
            Renderer.Expand(image, ds, ds);
            Renderer.Expand(save_export_button, ds, ds);
        }
        public void Shrink(int ds)
        {
            Renderer.Shrink(image, ds, ds);
            Renderer.Shrink(save_export_button, ds, ds);
        }

        public PictureBox Image { get { return image; } }

        //public Button GetSaveExportButton() { return save_export_button; }

        private PictureBox image;

        private Button save_export_button;
    };

}
