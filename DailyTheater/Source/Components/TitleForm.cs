﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class TitleForm : Form
    {
        public TextBox textbox { get; set; }
        private Button okbutton;

        public TitleForm()
        {
            // Initialize
            Text = "제목을 입력해주세요.";
            Size = new Size(300, 80);
            ControlBox = false;
            StartPosition = FormStartPosition.CenterParent;
            FormBorderStyle = FormBorderStyle.FixedDialog;

            textbox = new TextBox()
            {
                Location = Location + new Size(10, 10),
                Size = new Size(200, 25),
                ImeMode = ImeMode.Hangul,
            };
            okbutton = new Button()
            {
                Text = "OK",
                Location = Location + new Size(220, 10),
                Size = new Size(55, 22),
            };
            AcceptButton = okbutton;

            // Register events
            okbutton.Click += Okbutton_Click;

            // Add controls
            Controls.Add(textbox);
            Controls.Add(okbutton);
        }

        private void Okbutton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }

}
