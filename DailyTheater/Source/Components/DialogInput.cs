﻿using DailyTheater;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class DialogInput
    {
        public DialogInput(Timeline timeline, int width, int height)
        {
            // Initialize
            size = new Size(width, height);
            timeline_observer = timeline;

            characterButton = new CharacterButton(width / 2, height / 2)
            {
                Name = "character button"
            };
            pictureBox = new PictureBox()
            {
                Image = Properties.Resources.input,
                Size = new Size(690, 155),
                Location = new Point(0, 0),
                SizeMode = PictureBoxSizeMode.StretchImage,
            };
            text = new DivideLine(pictureBox);

            // Register events
            text.Current.KeyDown += Text_KeyDown;

            // Add controls
            pictureBox.Controls.Add(characterButton);
            pictureBox.Controls.Add(text.Current);
        }

        public static T Get<T>(Control control, string itemName) where T : Control
        {
            var result = control.Controls.Find(itemName, true);
            return (result.Length > 0) ? result[0] as T : null;
        }

        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && !e.Control)
            {
                var dialog = (CharacterButton)characterButton.Clone();
                dialog.Name = "dialog";

                var clonetext = text.Clone() as DivideLine;

                var name = new Label()
                {
                    Text = dialog.ComboBoxName,
                    Location = new Point(57, 30),
                    Size = new Size(100, 20),
                    Font = Globals.textFont,
                    BackColor = Color.Transparent,
                    Name = "name",
                    ForeColor = Color.FromArgb(0x4a, 0x4a, 0x4a),
                };
                name.Paint += (s, _e) =>
                {
                    _e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                };

                var deleteButton = new Button()
                {
                    Text = "삭제",
                    Size = new Size(50, 25),
                    Location = new Point(600, 20),
                };
                deleteButton.Click += (s, arg) =>
                {
                    var btn = s as Control;
                    timeline_observer.Dialogs.Controls.Remove(btn.Parent);
                };

                var borderline = new PictureBox()
                {
                    Location = new Point(0, 80 + clonetext.Current.Height),
                    Size = new Size(690, 10),
                    Image = Properties.Resources.borderline,
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    BackColor = Color.FromArgb(0xfc, 0xfc, 0xfe),
                    Name = "borderline",
                };

                var picturebox = new Panel()
                {
                    //Image = Properties.Resources.input,
                    //SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage,
                    Size = new Size(690, 70 + clonetext.Current.Height + 20),
                    BackColor = Color.Transparent,
                    Name = "block",
                };

                picturebox.Controls.Add(borderline);
                picturebox.Controls.Add(dialog);
                picturebox.Controls.Add(name);
                picturebox.Controls.Add(clonetext.Current);
                picturebox.Controls.Add(deleteButton);

                dialog.BringToFront();
                name.BringToFront();
                clonetext.Current.BringToFront();
                deleteButton.BringToFront();
                borderline.BringToFront();

                string partner = timeline_observer.LastSpeeker;
                var typewriter = timeline_observer.Dialogs.Controls[timeline_observer.Dialogs.Controls.Count - 1];

                timeline_observer.Dialogs.Controls.Remove(typewriter);
                timeline_observer.Dialogs.Controls.Add(picturebox);
                timeline_observer.Dialogs.Controls.Add(typewriter);

                text.ChangeCurrent();
                if (partner != null)
                {
                    Get<CharacterButton>(typewriter, "character button").ComboBoxName = partner;
                    text.Current.Focus();
                }
                else
                {
                    Get<CharacterButton>(typewriter, "character button").ComboBoxFocus();
                }
            }
        }

        public CharacterButton characterButton { get; }

        public DivideLine text { get; }

        public Size size { get; set; }

        public PictureBox pictureBox { get; }

        private Timeline timeline_observer;
    }

    //public class Line : ICloneable
    public class DivideLine
    {
        public DivideLine(Control parent)
        {
            // Initialize
            textbox = new TextBox()
            {
                Name = "textbox",
                ImeMode = ImeMode.Hangul,
                Multiline = true,
                BackColor = Color.FromArgb(0xaf, 0xaf, 0xaf),
                BorderStyle = BorderStyle.None,
                Location = new Point(10, 75),
                Font = Globals.textFont,
                Size = new Size(670, 20),
            };
            line = new Label()
            {
                Name = "line",
                Location = new Point(0, 75),
                Size = new Size(670, 20),
                BackColor = Color.Transparent,
                ForeColor = Color.FromArgb(0x4a, 0x4a, 0x4a),
                Font = Globals.textFont,
            };

            current = textbox;
            observer = parent;

            // Register events
            line.Paint += (s, e) =>
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            };
            textbox.KeyDown += Textbox_KeyDown;
            line.Click += Line_Click;
        }

        public object Clone()
        {
            var clone = new DivideLine(observer)
            {
                Observer = observer
            };
            clone.current = clone.line;
            clone.textbox.Location = new Point(10, 75);
            clone.textbox.Text = textbox.Text;
            clone.line.BackColor = Color.FromArgb(0xfc, 0xfc, 0xfe);
            clone.line.Text = line.Text;

            return clone;
        }

        public void ChangeCurrent()
        {
            ChangeCurrent(textbox);
            line.Height = 20;
        }

        private void ChangeCurrent(Control next)
        {
            observer.Controls.Remove(current);
            observer.Controls.Add(current = next);
            current.BringToFront();
        }

        private void Line_Click(object sender, EventArgs e)
        {
            textbox.Text = line.Text;
            line.Height = 20;
            ChangeCurrent(textbox);
        }

        private void Textbox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = (e.KeyData != (Keys.Control | Keys.Enter));
                if (e.Control)
                {
                    line.Height = (textbox.Height += textbox.Height < 60 ? 20 : 0);
                }
                else
                {
                    line.Text = textbox.Text;
                    textbox.Text = "";
                    ChangeCurrent(line);
                }
            }
            else if (e.KeyCode == Keys.Back && textbox.Text.Length > 0 && textbox.Text.EndsWith(Environment.NewLine))
                line.Height = (textbox.Height -= textbox.Text.Count(c => c == '\n') < 3 ? 20 : 0);
        }

        public Control Current { get { return current; } set { current = value; } }
        
        public Control Observer { set { observer = value; } }
        
        private TextBox textbox;
        
        private Label line;
        
        private Control current;
        
        private Control observer;
    }
}
