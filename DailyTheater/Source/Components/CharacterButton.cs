﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace DailyTheater.Components
{
    public class CharacterButton : Button, ICloneable
    {
        static Dictionary<string, string> characters = new Dictionary<string, string>()
        {
            /* μ's */
            {"maki",        "마키"},
            {"rin",         "린"},
            {"hanayo",      "하나요"},
            {"umi",         "우미"},
            {"honoka",      "호노카"},
            {"kotori",      "코토리"},
            {"eri",         "에리"},
            {"niko",        "니코"},
            {"nozomi",      "노조미"},
            /* aqours */
            {"yoshiko",     "요시코"},
            {"haanmaru",    "하나마루"},
            {"ruby",        "루비"},
            {"riko",        "리코"},
            {"chika",       "치카"},
            {"you",         "요우"},
            {"kanan",       "카난"},
            {"dia",         "다이아"},
            {"mari",        "마리"},
            /* nijigasaki */
            {"sizuku",      "시즈쿠"},
            {"kasumi",      "카스미"},
            {"rina",        "리나"},
            {"ai",          "아이"},
            {"ayumu",       "아유무"},
            {"setsuna",     "세츠나"},
            {"kanata",      "카나타"},
            {"karin",       "카린"},
            {"emma",        "엠마"},
            {"shioriko",    "시오리코"},
        };

        public CharacterButton(int width, int height)
        {
            // Initialize
            Size = new Size(width, height);
            FlatStyle = FlatStyle.Flat;
            BackColor = Color.Transparent;
            TabStop = false;
            FlatAppearance.BorderSize = 0;
            FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            FlatAppearance.MouseOverBackColor = BackColor;
            FlatAppearance.MouseDownBackColor = BackColor;

            combobox = new ComboBox()
            {
                Visible = false,
                //Location += new Size(57, 30),
                Size = new Size(70, 20),
                ImeMode = ImeMode.Hangul,
                //DropDownHeight = combobox.ItemHeight * 9,
            };
            combobox.Location += new Size(57, 30);
            combobox.DropDownHeight = combobox.ItemHeight * 9;

            portrait = new PictureBox()
            {
                InitialImage = null,
                Size = new Size(160 / 3, 160 / 3),
                //Location += new Size(0, 10),
                //Size = new Size(160, 160),
            };
            //portrait.InitialImage = null;
            //portrait.Size = new Size(160 / 3, 160 / 3);
            portrait.Location += new Size(0, 10);

            //portrait.Size = new Size(160, 160);
            //portrait.Location -= new Size(10, 20);

            // Register events
            MouseClick += CharacterButton_MouseClick;
            BackColorChanged += (s, e) =>
            {
                FlatAppearance.MouseOverBackColor = BackColor;
                FlatAppearance.MouseDownBackColor = BackColor;
            };
            combobox.SelectedValueChanged += Combobox_SelectedValueChanged;
            combobox.TextChanged += Combobox_TextChanged;
            combobox.KeyDown += Combobox_KeyDown;

            // Add controls
            foreach (var character in characters)
                combobox.Items.Add(character.Value);

            Controls.Add(combobox);
            Controls.Add(portrait);
        }

        private void Combobox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                foreach (var character in characters)
                    combobox.Items.Add(character.Value);

                combobox.SelectedItem = combobox.Text;
                combobox.DroppedDown = false;
            }
        }

        // https://stackoverflow.com/questions/17731890/i-want-to-stop-combobox-items-clear-from-moving-my-text-cursor
        public static void SafeClearItems(ComboBox comboBox)
        {
            foreach (var item in new ArrayList(comboBox.Items))
            {
                comboBox.Items.Remove(item);
            }
        }

        private void Combobox_TextChanged(object sender, EventArgs e)
        {
            if (combobox.Text == combobox.SelectedItem?.ToString())
                return;

            combobox.DroppedDown = true;
            SafeClearItems(combobox);
            foreach (var character in characters)
            {
                if (character.Value.Contains(combobox.Text) || character.Key.Contains(combobox.Text))
                    combobox.Items.Add(character.Value);
            }
        }

        public object Clone()
        {
            var clone = new CharacterButton(Size.Width, Size.Height);
            clone.combobox.Items.AddRange(combobox.Items.Cast<object>().ToArray());
            clone.combobox.SelectedItem = combobox.SelectedItem;
            clone.combobox.Location = new Point(150, 30);
            clone.portrait.Image = portrait.Image;
            return clone;
        }

        static public Rectangle getCropArea(string name)
        {
            int idx = characters.Values.ToList().IndexOf(name);
            List<Point> startPos = new List<Point>()
            {
                new Point(0,0), new Point(480,0), new Point(0,480), new Point(480,480)
            };
            int group = idx / 9;
            int number = idx % 9;
            int grade = number / 3;
            return new Rectangle
            (
                new Point(startPos[group].X + 160 * (number % 3), startPos[group].Y + 160 * grade),
                new Size(160, 160)
            );
        }

        private void Combobox_SelectedValueChanged(object sender, EventArgs e)
        {
            portrait.Image = Properties.Resources.characters.Clone(getCropArea(combobox.SelectedItem.ToString()), System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            portrait.SizeMode = PictureBoxSizeMode.StretchImage;

            var name = Parent?.Controls.Find("name", true);
            if (name != null && name.Length > 0)
            {
                name[0].Text = combobox.SelectedItem.ToString();
            }
            combobox.DroppedDown = false;
        }

        private void CharacterButton_MouseClick(object sender, MouseEventArgs e)
        {
            combobox.Visible = true;
        }

        public void ComboBoxFocus() { combobox.Focus(); }

        public string ComboBoxName { get { return combobox.Text; } set { combobox.Text = value; } }

        private ComboBox combobox;

        private PictureBox portrait;
    }

}
