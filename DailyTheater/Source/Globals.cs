﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyTheater
{
    public static class Globals
    {
        public static readonly Font textFont;

        public static readonly Font titleFont;

        static Globals()
        {
            textFont = new Font("나눔명조 ExtraBold", 10f, FontStyle.Bold, GraphicsUnit.Point, 129);
            titleFont = new Font("나눔명조 ExtraBold", 14f, FontStyle.Bold, GraphicsUnit.Point, 129);
        }
    }
}
