﻿using DailyTheater.Components;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater.Helpers
{
    static class Renderer
    {
        private static void ForEach(Control control, Bitmap bitmap, Point loc, Action<Control, Bitmap, Point> execute)
        {
            execute(control, bitmap, loc);
            foreach (Control c in control.Controls)
                ForEach(c, bitmap, loc, execute);
        }

        public static void ForEach(Control control, int dx, int dy, Action<Control, int, int> execute)
        {
            execute(control, dx, dy);
            foreach (Control c in control.Controls)
                ForEach(c, dx, dy, execute);
        }

        private static void Draw(Control control, Bitmap bitmap, Point loc)
        {
            if (control is CharacterButton || control is Label || control.Name == "borderline")
            {
                var curloc = control.PointToScreen(Point.Empty);
                control.DrawToBitmap(bitmap, new Rectangle(curloc.X - loc.X + 135, curloc.Y - loc.Y, control.Width, control.Height));
            }
        }

        // https://stackoverflow.com/questions/1720160/how-do-i-fill-a-bitmap-with-a-solid-color
        public static Bitmap DrawDialog(Panel dialog)
        {
            Bitmap bitmap = new Bitmap(2346, dialog.Height);
            using (Graphics gfx = Graphics.FromImage(bitmap))
            using (SolidBrush brush = new SolidBrush(Color.FromArgb(0xfc, 0xfc, 0xfe)))
            {
                gfx.FillRectangle(brush, 0, 0, bitmap.Width, bitmap.Height);
            }
            var loc = dialog.PointToScreen(Point.Empty);
            ForEach(dialog, bitmap, loc, Draw);
            return bitmap;
        }

        public static void Expand(Control control, int dx, int dy)
        {
            if (control is Label)
            {
                var label = control as Label;
                var size = label.Font.Size * (label.Name == "title" ? 3 : 4);
                label.Font = new Font("나눔명조 ExtraBold", size, FontStyle.Bold, GraphicsUnit.Point, 129);
                label.ForeColor = Color.FromArgb(0x4a, 0x4a, 0x4a);
            }
            control.Size = new Size(control.Width * dx, control.Height * dy);
            control.Location = new Point(control.Location.X * dx, control.Location.Y * dy);
        }

        public static void Shrink(Control control, int dx, int dy)
        {
            if (control is Label)
            {
                var label = control as Label;
                var size = label.Font.Size / (label.Name == "title" ? 3 : 4);
                label.Font = new Font("나눔명조 ExtraBold", size, FontStyle.Bold, GraphicsUnit.Point, 129);
                label.ForeColor = Color.FromArgb(0x4a, 0x4a, 0x4a);
            }
            control.Size = new Size(control.Width / dx, control.Height / dy);
            control.Location = new Point(control.Location.X / dx, control.Location.Y / dy);
        }
    }
}
