﻿using DailyTheater.Components;
using DailyTheater.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DailyTheater.Source.Helpers
{
    static class FileHelper
    {
        public static void SaveFile()
        {
            throw new NotImplementedException();

            using SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            dialog.FilterIndex = 2;
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                using Stream stream = dialog.OpenFile();
                // Save data
            }
        }

        public static void Export(Header header, Footer footer, Timeline timeline)
        {
            int height = (100 + 59) * 3;
            var dialogs = new List<Bitmap>();
            var background = header.Image;
            var title = header.Title;

            title.Width = 770;
            header.Expand(3);

            var bitmap = new Bitmap(background.Width, 100 * 3);
            background.DrawToBitmap(bitmap, new Rectangle(0, 0, background.Width, background.Height));
            title.DrawToBitmap(bitmap, new Rectangle(8 * 3, 50 * 3, title.Width, title.Height));
            bitmap.Tag = "header";
            dialogs.Add(bitmap);
            header.Shrink(3);
            title.Width = 784;

            timeline.Expand(3);

            var typewriter = timeline.Dialogs.Controls[timeline.Dialogs.Controls.Count - 1];
            timeline.Dialogs.Controls.Remove(typewriter);

            foreach (Panel dialog in timeline.Dialogs.Controls)
            {
                height += dialog.Height;
                dialog.Controls.Find("dialog", true)[0].BringToFront();
                dialogs.Add(Renderer.DrawDialog(dialog));
                dialog.Controls.Find("borderline", true)[0].BringToFront();
                dialog.Controls.Find("name", true)[0].BringToFront();
                dialog.Controls.Find("line", true)[0].BringToFront();
            }
            timeline.Dialogs.Controls.Add(typewriter);
            timeline.Shrink(3);
            typewriter.Focus();

            footer.Expand(3);
            background = footer.Image;
            bitmap = new Bitmap(background.Width, background.Height);
            background.DrawToBitmap(bitmap, new Rectangle(0, 0, background.Width, background.Height));

            var bm = bitmap.Clone(new Rectangle(0, background.Height - 59 * 3, background.Width, 59 * 3), System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            bm.Tag = "footer";
            dialogs.Add(bm);

            var result = new Bitmap(background.Width, height);
            int heightsum = 0;

            foreach (var dialog in dialogs)
            {
                using Graphics g = Graphics.FromImage(result);
                g.DrawImage(result, 0, 0);
                if (dialog.Tag == null)
                {
                    g.DrawImage(dialog, 0, 100 * 3 + heightsum);
                    heightsum += dialog.Height;
                }
                else
                {
                    if (dialog.Tag.ToString() == "header")
                        g.DrawImage(dialog, 0, 0);
                    else
                        g.DrawImage(dialog, 0, height - 59 * 3);
                }
            }
            footer.Shrink(3);

            var titleText = title.Text.Trim('『', '』');
            result.Save($"{DateTime.Today.ToShortDateString()}_{titleText}.png");
        }
    }
}
