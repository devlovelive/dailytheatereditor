﻿using DailyTheater.Components;
using DailyTheater.Helpers;
using DailyTheater.Source.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DailyTheater
{
    public partial class MainForm : Form
    {
        private Header header;
        private Footer footer;
        private Timeline timeline;

        public MainForm()
        {
            // Initialize
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;

            header = new Header();
            footer = new Footer();
            timeline = new Timeline(Size);

            // Register events
            createNewToolStripMenuItem.Click    += CreateNewToolStripMenuItemOnClick;
            saveToolStripMenuItem.Click         += SaveToolStripMenuItemOnClick;
            saveAsToolStripMenuItem.Click       += SaveAsToolStripMenuItemOnClick;
            loadToolStripMenuItem.Click         += LoadToolStripMenuItemOnClick;
            exportToolStripMenuItem.Click       += ExportToolStripMenuItemOnClick;

            gothicFontToolStripMenuItem.Click   += GothicFontToolStripMenuItemOnClick;
            monoFontToolStripMenuItem.Click     += MonofontToolStripMenuItemOnClick;

            programInfoToolStripMenuItem.Click  += ProgramInfoToolStripMenuItemOnClick;

            // Add controls
            Controls.Add(header.Image);
            Controls.Add(header.Title);
            header.Title.BringToFront();

            Controls.Add(footer.Image);
            //Controls.Add(footer.GetSaveExportButton());
            //footer.GetSaveExportButton().BringToFront();

            Controls.Add(timeline.Dialogs);
            timeline.Dialogs.BringToFront();
        }

        //
        // File ToolStripMenu
        //
        private void CreateNewToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            using var titleform = new TitleForm();
            titleform.ShowDialog();

            if (titleform.textbox.Text.Length > 0)
                header.Title.Text = ($"『{titleform.textbox.Text}』");
        }

        private void SaveToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            FileHelper.SaveFile();
        }

        private void SaveAsToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
            FileHelper.SaveFile();
        }

        private void LoadToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ExportToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            if (timeline.Dialogs.Controls.Count == 0)
            {
                MessageBox.Show("대화가 없습니다!");
                return;
            }
            FileHelper.Export(header, footer, timeline);
            MessageBox.Show("내보내기 완료");
        }

        //
        // Font ToolStripMenu
        //
        private void GothicFontToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void MonofontToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        //
        // ProgramInfo ToolStripMenu
        //
        private void ProgramInfoToolStripMenuItemOnClick(object sender, EventArgs e)
        {
            MessageBox.Show("요하네", "5집센터", MessageBoxButtons.OK);
        }
    }
}
