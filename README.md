# Daily Theater Editor #

LLAS!의 매일 극장 번역을 위한 에디터 프로그램입니다.   
[다운로드 링크](https://bitbucket.org/dev-love-live/dailytheatereditor/downloads/)

### 실행 환경 ###

* .NET Runtime 5.0.x

### 사용 방법 ###

1. DailyTheater.exe 파일 실행
2. 상단 메뉴에서 '파일 > 새로 만들기' 클릭
> 필요한 경우, 상단 메뉴에서 '폰트' 클릭 후 기본 폰트 또는 고정폭 폰트 선택 가능
3. 제목 입력 후 캐릭터 선택하여 텍스트 작성
4. 상단 메뉴에서 '파일 > 내보내기' 클릭하여 .png 형식 파일로 내보내기
5. 내용 저장은 상단 메뉴에서 '파일 > 저장' 클릭, 불러오기는 '파일 > 불러오기' 클릭

### 주의사항 ###

* 모든 이미지 리소스는 기존에 사용되던 양식을 바탕으로 하고 있습니다.
* 해당 프로그램을 사용해 높은 수위의 SS를 작성하는 일은 지양해주세요.

### Contribution guidelines ###

* Visual Studio 2019, .NET 5.0 및 C# 9.0을 기준으로 개발하고 있습니다.
* 동작이 답답하거나 추가하고 싶은 기능이 있으면 [Issues](https://bitbucket.org/dev-love-live/dailytheatereditor/issues)에 올려주세요.
* 코드 답답하면 직접 Branch 새로 파서 PR 날리세요.
* 코딩 스타일은 기존 방식을 따라 주세요.
